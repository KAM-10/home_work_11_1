package Task_5;

/*Task5:
        Create an abstract class called "Animal" with an abstract method called "makeSound()".
        Implement this abstract class by creating three subclasses: "Dog," "Cat," and "Cow."
        Each subclass should implement the "makeSound()" method to print a unique sound for that animal.
        Create objects of all three subclasses and call the "makeSound()" method on each object
        to hear the animal sounds.*/


public class Creator {
    public static void main(String[] args) {
        Dog dog = new Dog();
        Cat cat = new Cat();
        Cow cow = new Cow();
        cow.makeSound();
        cat.makeSound();
        dog.makeSound();
    }
}
