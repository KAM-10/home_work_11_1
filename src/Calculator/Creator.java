package Calculator;

import java.util.Scanner;

public class Creator {
    public static void main(String[] args) {
        Calculator fun = new Calculator();
        Scanner input = new Scanner(System.in);

        System.out.print("Enter first number: ");
        int num1 = input.nextInt();
        System.out.print("Enter second number: ");
        int num2 = input.nextInt();
        System.out.print("Enter your process: ");
        String symbol = input.next();

        switch (symbol) {
            case "+":
                System.out.println("Result: " + fun.sum(num1,num2));
                break;

            case "-":
                System.out.println("Result: " + fun.subtraction(num1,num2));
                break;


            case "*":
                System.out.println("Result: " + fun.multiplication(num1,num2));
                break;

            case "/":
                System.out.println("Result: " + fun.divide(num1,num2));
                break;

            default:
                System.out.println("I don't understand your process ");
                break;
        }



    }
}