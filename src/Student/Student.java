package Student;

public class Student {
    private String name;
    private int age;
    private int garde;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getGarde() {
        return garde;
    }

    public void setGarde(int garde) {
        this.garde = garde;
    }
    public void information(){
        System.out.println("My name is " + getName() + " I'am " + getAge() + " years and " + getGarde() + " class." );
    }
}
