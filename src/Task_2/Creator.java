package Task_2;

/*
Task2:
        Create an abstract class called Employee with properties name and salary. Implement the following methods:
        getDetails(): Prints the employee's name and salary.
        Create the following subclasses that inherit from Employee:
        Manager: Representing a manager with an additional property department. Implement the getDetails() method to include the department information.
        Developer: Representing a developer with an additional property programmingLanguage. Implement the getDetails() method to include the programming language information.
*/

public class Creator {
    public static void main(String[] args) {
        Developer developer = new Developer("Kamran",2500,"Java");
        Manager manager = new Manager("Veli",1500,"Drive");
        manager.getDetails();
        developer.getDetails();

    }
}
