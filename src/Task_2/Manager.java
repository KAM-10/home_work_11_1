package Task_2;

class Manager extends Employee {
    private String department;

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Manager(String name, double salary, String department) {
        super(name, salary);
        this.department = department;
    }

    @Override
    public void getDetails() {
        System.out.println("Name " + getName() + " Salary " + getSalary() + " Program Language " + getDepartment());

    }
}
