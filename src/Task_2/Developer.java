package Task_2;

public class Developer extends Employee{
    private String programLanguage;

    public String getProgramLanguage() {
        return programLanguage;
    }

    public void setProgramLanguage(String programLanguage) {
        this.programLanguage = programLanguage;
    }

    public Developer(String name, double salary, String programLanguage) {
        super(name, salary);
        this.programLanguage = programLanguage ;
    }

    @Override
    public void getDetails() {
        System.out.println("Name " + getName() + " Salary " + getSalary() + " Program Language " + getProgramLanguage());
    }
}
