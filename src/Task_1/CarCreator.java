package Task_1;
/*
        Task1:
        Create a Car class with private properties:
        make (String),
        model (String),
        year (int),
        rentalPrice (double).
        Use encapsulation principle for this properties. Ensure that the properties cannot be accessed directly from outside the class

*/
public class CarCreator {
    public static void main(String[] args) {
        Car car = new Car();
        car.setMake("Drive");
        car.setModel("BMW");
        car.setYear(2013);
        car.setRentalPrice(100);
        System.out.println(car.getMake() + " " + car.getModel() + " " + car.getYear() + " " + car.getRentalPrice());
    }
}
