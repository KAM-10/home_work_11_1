package Drive;

public class Motorcycle extends Vehicle {
    @Override
    public void startEngine(){
        System.out.println("Motorcycle Start");
    }
    @Override
    public void drive(){
        System.out.println("Motorcycle Drive");
    }
}
