package Drive;

public class Truck extends Vehicle {
    @Override
    public void startEngine(){
        System.out.println("Truck Start");
    }
    @Override
    public void drive(){
        System.out.println("Truck Drive");
    }
}
