package Task_6;

public class Teacher extends Person {

    private String subjectTaught;

    public String getSubjectTaught() {
        return subjectTaught;
    }

    public void setSubjectTaught(String subjectTaught) {
        this.subjectTaught = subjectTaught;
    }

    public Teacher(String name, int age,String subjectTaught){
        super(name,age);
        this.subjectTaught = subjectTaught;
    }
    @Override
    public void displayInfo() {
        System.out.println("My name is " + getName() + " I'am " + getAge() + " years old." + " Today i teaching " + getSubjectTaught());

    }

}
