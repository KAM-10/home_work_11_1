package Task_6;

public class Student extends Person{
    private int StudentID;

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        StudentID = studentID;
    }

    public Student(String name, int age, int studentID){
        super(name,age);
        this.StudentID = studentID;
    }
    @Override
    public void displayInfo() {
        System.out.println(getStudentID() + ": My name is " + getName() + " I'am " + getAge() + " years old" );

    }
}
