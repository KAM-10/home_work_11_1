package Task_6;

/*
Task6:
        Create an abstract class called "Person" with instance variables for name and age.
        Define getter and setter methods for these variables.
        Include an abstract method called "displayInfo()" that displays the person's name and age.
        Implement this abstract class by creating two subclasses: "Student" and "Teacher".
        Override the "displayInfo()" method in each subclass to display additional information
        specific to that role (e.g., student ID for the "Student" class, subject taught for the
        "Teacher" class). Create objects of both subclasses and call the "displayInfo()" method on
        each object to see the corresponding information.
*/


public class Creator {
    public static void main(String[] args) {
        Teacher teacher = new Teacher("Tofiq",26,"OOP");
        Student student = new Student("Kamran",29,1);
        teacher.displayInfo();
        student.displayInfo();

    }
}
