package Task_3;

public interface Playable {
    void play();
    void stop();

}
