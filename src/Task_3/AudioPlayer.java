package Task_3;

public class AudioPlayer implements Playable {

    @Override
    public void play() {
        System.out.println("Player Audio");
    }

    @Override
    public void stop() {
        System.out.println("Stop Audio");
    }
}
