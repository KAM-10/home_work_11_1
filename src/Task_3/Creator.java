package Task_3;

public class Creator {
    public static void main(String[] args) {
        VideoPlayer videoPlayer = new VideoPlayer();
        AudioPlayer audioPlayer = new AudioPlayer();
        videoPlayer.play();
        audioPlayer.play();
        videoPlayer.stop();
        audioPlayer.stop();
    }
}
